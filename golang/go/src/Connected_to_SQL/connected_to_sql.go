package main

import(
   "database/sql"
   _"github.com/Go-SQL-Driver/MySQL"//就是你下载的文件地址，如果是自己拷贝的，那么就写自己创建的路径
   "log"
   "fmt"
   "time"
)

func main() {
   //username mysql账号
   var userName = "root"
   //password mysql密码
   var passWord = "L15169037873q"
   //ip   mysql数据库的IP
   var ip = "localhost"
   //port  mysql数据库的端口
   var port = "3306"
   // database 需要连接的数据库名称
   var database = "video"

   dataSourceName := sourceName2(userName,passWord ,ip ,port ,database)
   //连接示例
   // db,err := sql.Open("mysql","test2:abc@tcp(192.168.136.136:3306)/test?charset=utf8" )
   conn,err := sql.Open("mysql",dataSourceName )
   if err != nil{
      panic(err.Error())
      log.Println(err)
      return
   }else {
      fmt.Println("connection mysql succcess ! ")
   }
   defer conn.Close()  //只有在前面用了 panic[抛出异常] 这时defer才能起作用，如果链接数据的时候出问题，他会往err写数据。defer:延迟，这里立刻申请了一个关闭sql 链接的草错，defer 后的方法，或延迟执行。在函数抛出异常一会被执行


   //产生查询语句的Statement
   stmt, err := conn.Prepare(`show tables`)
   if err != nil {
      log.Fatal("Prepare failed:", err.Error())
   }
   defer stmt.Close()

   //通过Statement执行查询
   rows, err := stmt.Query()
   if err != nil {
      log.Fatal("Query failed:", err.Error())
   }

   //建立一个列数组
   cols, err := rows.Columns()
   var colsdata = make([]interface{}, len(cols))
   for i := 0; i < len(cols); i++ {
      colsdata[i] = new(interface{})
      fmt.Print(cols[i])
      fmt.Print("\t")
   }
   fmt.Println()

   //遍历每一行
   for rows.Next() {
      rows.Scan(colsdata...) //将查到的数据写入到这行中
      PrintRow(colsdata)     //打印此行
   }
   defer rows.Close()
}
//连接到mysql
func sourceName2(userName , passWord ,ip , port, database string) string{
   var connection string
   connection =  userName + ":" + passWord +"@tcp(" + ip + ":" + port + ")/"+database+"?charset=utf8"
   return  connection
}
//打印一行记录，传入一个行的所有列信息
func PrintRow(colsdata []interface{}) {
   for _, val := range colsdata {
      switch v := (*(val.(*interface{}))).(type) {
      case nil:
         fmt.Print("NULL")
      case bool:
         if v {
            fmt.Print("True")
         } else {
            fmt.Print("False")
         }
      case []byte:
         fmt.Print(string(v))
      case time.Time:
         fmt.Print(v.Format)
      default:
         fmt.Print(v)
      }
      fmt.Print("\t")
   }
   fmt.Println()
}