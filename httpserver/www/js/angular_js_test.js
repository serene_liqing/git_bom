var app = angular.module('myApp', []);

app.controller('nameCtrl', function($scope) {
    $scope.names = ["Google", "W3Cschool", "Taobao"];
});

app.controller('siteCtrl',function($scope){
    $scope.sites=[
        {site:"Google",url:"http://www.google.com"},
        {site:"W3Cschool",url:"http://www.w3cschool.cn"},
        {site:"Taobao",url:"http://www.taobao.com"}
    ];
});

app.controller("carCtrl",function($scope){
    $scope.cars={
        car01:{brand:"Ford", model:"Mustang", color:"red"},
        car02:{brand:"Fiat", model:"500", color:"white"},
        car03:{brand:"Volvo", model:"XC90", color:"black"}
        // brand model 和 color之间以","分隔
    }
});

app.controller("petCtrl",function($scope){
    $scope.pets={
        pet1:{brand:"dog",name:"Bally"},
        pet2:{brand:"cat",name:"Cream"},
        pet3:{brand:"tortoise",name:"calm"}
        //pet model之间用","去分隔
    }
});

angular.module('myApp',[]).controller('userCtrl',function($scope){
    $scope.fName='';
    $scope.lName='';
    $scope.passw1='';
    $scope.passw2='';
    $scope.users=[{id:1,fName:'Hege',lName:'Pege'},
                  {id:2,fName:'Kim',lName:'Pim'},
                  {id:3,fName:'Sal',lName:'Smith'},
                  {id:4,fName:'Jack',lName:'Jones'},
                  {id:5,fName:'John',lName:'Doe'},
                  {id:6,fName:'Wang',lName:'Loen'}
    ];
    $scope.edit = true;
    $scope.error = false;
    $scope.incomplete = false;
    $scope.editUser = function(id){

        if(id=='new'){

            $scope.edit=true;
            $scope.incomplete=true;
            $scope.fName='';
            $scope.lName='';

        }else{
            $scope.edit=false;
            $scope.fName=$scope.users[id-1].fName;
            $scope.lName=$scope.users[id-1].lName;
        }
    };
    $scope.$watch('passw1',function(){$scope.test();});
    $scope.$watch('passw2',function(){$scope.test();});
    $scope.$watch('fName',function(){$scope.test();});
    $scope.$watch('lName',function(){$scope.test();});

    $scope.test=function(){
        if($scope.passw1!==$scope.passw2){
            $scope.error = true;
        }else{
            $scope.error = false;
        }
        $scope.incomplete = false;
        if($scope.edit&&(!$scope.fName.length||!$scope.lName.length||!$scope.passw1.length||!$scope.passw2.length)){
            $scope.incomplete=true;
        }
    };
})


