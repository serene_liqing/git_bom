var app = new Vue({
    el: '#app',
    data: {
      name: 'Vue.js',
      user: "admin",
      password: ""
    },
    // 在 `methods` 对象中定义方法
    methods: {
      doLogin: function (event) {
        // `this` 在方法里指当前 Vue 实例
        axios.post('/video/login', {
          user: this.user,
          password: this.password
        })
        .then(function(res){
           if (res.data == "success"){
             window.location.href="index.html"
           }

        })
      }
    }
  })