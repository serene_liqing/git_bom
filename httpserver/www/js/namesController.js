var app= angular.module('myApp',[]);

app.controller('namesCtrl',function($scope){
$scope.names = [
 {name:'Jani',country:'Norway'},
 {name:'Hege',country:'Sweden'},
 {name:'Kai',country:'Denmark'}
];
});

app.controller('myCtrl',function($scope,$location){
    $scope.myUrl=$location.absUrl();
});

app.controller('personCtrl',function($scope){
    $scope.firstName='Serena';
    $scope.lastName='Li';
    $scope.fullName=function(){
        return $scope.firstName+" "+$scope.lastName;
    }
});

