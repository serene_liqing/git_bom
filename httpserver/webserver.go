package main

import(
	"fmt"
    "net/http"
    "io"
    "os"
    "io/ioutil"
    "strings"
    "encoding/json"
    "github.com/gorilla/sessions"
    "time"
)
/////////////////////////////////////////////////////////////////////////////
var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key = []byte("super-secret-key")
	store = sessions.NewCookieStore(key)
)
/////////////////////////////////////////////////////////////////////////////
type WebAuth struct {
    User   string      
    Password string 
}
/////////////////////////////////////////////////////////////////////////////
func readFile(file_name  string) ([]byte, error){
    fi, err := os.Open(file_name)
    if err != nil{
        panic(err)
    } 

    defer fi.Close()
    return ioutil.ReadAll(fi)

}
func isStaticPage(url string) bool{

    var staticSuffix[13]string
    var i int
    staticSuffix[0]  = ".htm"
    staticSuffix[1]  = ".html"
    staticSuffix[2]  = ".xhtml"
    staticSuffix[3]  = ".css"
    staticSuffix[4]  = ".js"
    staticSuffix[5]  = ".jpg"
    staticSuffix[6]  = ".jpeg"
    staticSuffix[7]  = ".png"
    staticSuffix[8]  = ".gif"
    staticSuffix[9]  = ".ico"
    staticSuffix[10] = ".eot"
    staticSuffix[11] = ".ttf"
    staticSuffix[12] = ".woff"
    

    for i=0; i<13; i++{
        if ( strings.HasSuffix(url, staticSuffix[i]) ) {
            return true
        }
    }
    return false
}
///////////////////////////////////////////////////////////////////////////
func handleHomePage(w http.ResponseWriter, req *http.Request){
    ResponseStaticFile(w, req, "login.html")
}
func GetPathFromUrl(url string) string{
    var prefix = "./www/"
    var path = prefix + url
    return path
}
func GetContentType(filename string) string{
    var mimeMap map[string]string
    
    mimeMap = make(map[string]string)
    mimeMap[".html"]  = "text/html"
    mimeMap[".htm"]   = "text/html"
    mimeMap[".css"]   = "text/css"
    mimeMap[".js"]    = "application/javascript"
    mimeMap[".jpg"]   = "image/jpeg"


    for suffix := range mimeMap{
        if( strings.HasSuffix(filename, suffix) ){
            return mimeMap[suffix]
        }
    }
    
    retstr := "text/html"
    return retstr
}

func ResponseStaticFile(w http.ResponseWriter, req *http.Request, url string){
    path := GetPathFromUrl(url)

    fi, err := os.Open(path)
    if err != nil{
        http.NotFound(w, req)
        return
    } 
    defer fi.Close()

    data, err := ioutil.ReadAll(fi)
    if err != nil{
        http.NotFound(w, req)
        return
    }
    content_type := GetContentType(path)
    w.Header().Set("Content-type", content_type)
    w.Write(data)

}
////////////////////////////////////////////////////////////////////////////
func upload(w http.ResponseWriter, r* http.Request){
   file, err := os.Create("./newFile")
   if err != nil {
       panic(err)
   }

   defer file.Close()

   _, err = io.Copy(file, r.Body)
   if err != nil {
       panic(err)
   }
   w.Write([]byte("upload success"))
}

//////////////////////////////////////////////////////////////////////////////
func httpHandler(w http.ResponseWriter, req *http.Request){
    var url =req.URL.Path
    fmt.Println(url)
    fmt.Println(req.URL.RawQuery)
    if ( url == "/" ){
        //w.Write([]byte("hello world"))
        session, _ := store.Get(req, "spaceXvideo")
        fmt.Println(session)
        if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
            handleHomePage(w, req)
           // http.Error(w, "Forbidden", http.StatusForbidden)
            return
        }else{
            http.Redirect(w, req, "/index.html", http.StatusFound)
            //ResponseStaticFile(w, req, "index.html")
        }
    } else {
        
        if ( isStaticPage(url) ) {
            ResponseStaticFile(w, req, url)
        }else if (url == "/space/login/"){
            handle_login(w, req);
        }else {
            //http.NotFound(w, req)
            fmt.Println(req.Method)
            fmt.Println(req.ContentLength)
            con, err_read := ioutil.ReadAll(req.Body)
            if (err_read != nil) {
               
            }else{
                fmt.Println("hello")
                fmt.Println(string(con))
            } 
        }
    }
  
}

func handle_login(w http.ResponseWriter, r *http.Request){
    if ( r.Method == http.MethodPost ){
       // sess := globalSessions.SessionStart(w, r)
        var auth WebAuth;
        con, err_read := ioutil.ReadAll(r.Body)
        if (err_read != nil) {

        }else{
            err := json.Unmarshal(con , &auth)
            if(err != nil){
            }else{
                if ( auth.User == "admin" && auth.Password == "1234" ){
                    
                    fmt.Println(auth.User)
                    fmt.Println(auth.Password)

                    session, _ := store.Get(r, "spaceXvideo")
                    fmt.Println(session)
                    if(session != nil){
                        fmt.Println("session")
                        session.Values["authenticated"] = true
                        session.Save(r, w)
                        w.Write([]byte("success"))
                        fmt.Println(session)
                    }
                }
            }
        } 
    }
}

func main() {
    t := time.Now()
    fmt.Println("******************************************************************")
    fmt.Println("                  Hello go web server")
    fmt.Println(t)
    fmt.Println("******************************************************************")
    http.HandleFunc("/", httpHandler)
    http.HandleFunc("/video/login", handle_login)
    http.ListenAndServe(":8000", nil)
    //select {} //阻塞进程
}

/////////////////////////////////////////////////////////////////////////////////////